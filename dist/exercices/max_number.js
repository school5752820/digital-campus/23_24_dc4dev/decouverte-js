export function maxNumber(array) {
    if (false === Array.isArray(array)) {
        return undefined;
    }
    if (array.length === 0) {
        return undefined;
    }
    let maxFound = Number.NEGATIVE_INFINITY;
    for (let value of array) {
        if (typeof value !== 'number') {
            continue;
        }
        if (value > maxFound) {
            maxFound = value;
        }
    }
    if (maxFound === Number.NEGATIVE_INFINITY) {
        return undefined;
    }
    return maxFound;
}
