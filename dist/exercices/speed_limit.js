export function speedLimit(speed) {
    if (typeof speed !== 'number') {
        return 'Error';
    }
    if (speed < 0) {
        return 'WTF';
    }
    if (speed <= 70)
        return 'Bonne route';
    if (speed < 75)
        return 'Fais gaffe';
    if (speed >= 120)
        return 'Police';
    let pointsCount = Math.floor((speed - 70) / 5);
    return `Vous avez perdu ${pointsCount} pts`;
}
