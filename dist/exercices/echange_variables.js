export function swapVariables(a, b) {
    // Solution 1
    [a, b] = [b, a];
    // Solution 2
    // let temp = a;
    // a = b;
    // b = temp;
    return { a, b };
}
