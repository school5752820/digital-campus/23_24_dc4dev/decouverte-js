export function isPortrait(width, height) {
    if (typeof width !== 'number' || typeof height !== 'number') {
        return undefined;
    }
    if (width === height) {
        return null;
    }
    return width < height;
}
export function isLandscape(width, height) {
    const result = isPortrait(width, height);
    if (typeof result === 'boolean') {
        return !result;
    }
    return result;
}
