import { truthyValues } from "./exercices/truthy_values.js";
// false, 0, undefined, null, '', NaN
const test1 = truthyValues([0, 1, false, true, null, undefined, '', 'bonjour', [], {}, NaN]);
console.log({ test1 });
