export function fizzBuzz(param: unknown): string {
  if (typeof param !== 'number') {
    return 'Error';
  }

  if (param % 3 === 0 && param % 5 === 0) {
    return 'fizzbuzz';
  }

  if (param % 3 === 0) return 'fizz';
  if (param % 5 === 0) return 'buzz';

  return param.toString();
}