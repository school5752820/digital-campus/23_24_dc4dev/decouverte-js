export function swapVariables(a: unknown, b: unknown): { a: unknown; b: unknown; } {
  // Solution 1
  [a, b] = [b, a];
  
  // Solution 2
  // let temp = a;
  // a = b;
  // b = temp;

  return { a, b };
}
