export function truthyValues(array: Array<unknown>): Array<unknown> {
  if (!Array.isArray(array)) {
    return [];
  }

  return array.filter((value) => {
    /*
    if (
      false === value ||
      0 === value ||
      undefined === value ||
      null === value ||
      '' === value ||
      Number.isNaN(value)
    ) {
      return false;
    }
    
    return true;
    */

    if (value) return true;

    return false;
  });
}