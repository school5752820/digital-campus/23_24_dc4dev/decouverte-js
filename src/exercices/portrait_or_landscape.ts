export function isPortrait(width: number, height: number): boolean|null|undefined {
  if (typeof width !== 'number' || typeof height !== 'number') {
    return undefined;
  }

  if (width === height) {
    return null;
  }

  return width < height;
}

export function isLandscape(width: number, height: number): boolean|null|undefined {
  const result = isPortrait(width, height);

  if (typeof result === 'boolean') {
    return !result;
  }

  return result;
}
